class Mouton :
    def init(self,gain_nour,x,y,ener,taux_reproduc,ca2,di1) :
        self.gain_nourriture = gain_nour
        self.position_x = x
        self.position_y = y
        self.energie = ener
        self.taux_reproduction = taux_reproduc

        self.carte2=ca2
        self.dimen=di1

    def variationEnergie(self,cf,i,j) :
        self.carte2=cf
        if self.carte2[i][j] == 0:
            self.energie = self.energie - 1
        else:
            self.energie = self.energie + self.gain_nourriture
        return self.energie

    def déplacement(self) :
        a=random.randint(-1,1)
        b=random.randint(-1,1)
        if a==-1 and  self.position_x==0:
            a=0
        if b==-1 and  self.position_y==0:
            b=0
        if a==1 and  self.position_x==self.dimen-1:
            a=0
        if b==1 and  self.position_y==self.dimen-1:
            b=0
        else:
            self.position_x=self.position_x+a
            self.position_y=self.position_y+b

    def place_mouton(self,i,j) :
        self.position_x = i
        self.position_y = j


    def nbMouton(self):
        c=0
        for i in range(self.dimension):
            for j in range(self.dimension):
                if self.position_x==i and self.position_y==j:
                    c=c+1
        return c
    def carte2(self,cf):
        self.carte2=cf

    def d_ener(self):
        return self.energie
  
