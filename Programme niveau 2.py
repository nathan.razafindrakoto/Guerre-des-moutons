class Loup:
    def init(self,gain_nour,x,y,taux_reproduction,Monde,Mouton):
        self.gain_nourriture = gain_nourriture
        self.position_x = x 
        self.position_y = y 
        self.energie = randint(1,gain_nourriture)
        self.taux_reproduction = taux_reproduction
        self.Monde = Monde
        self.Mouton = Mouton 


    def variationEnergie(self):
        x = 1
        manger = False
        mouton = None 
        for i in range (len(self.Mouton)):
            if self.Mouton[i].position_x == self.position_x and self.Mouton[i].position_y == self.position_y and x == i:
                self.energie += self.gain_nourriture
                x -= 1
                manger = True
                mouton = i 
            if manger == False:
                self.energie -= 1
            return self.energie,mouton


    def deplacement(self):
        self.position_x += randint(-1,1)
        self.position_y += randint(-1,1)
        
